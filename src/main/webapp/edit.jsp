<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; UTF-8" isELIgnored="false" pageEncoding="UTF-8" %>
<!doctype html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <title>Little Twitter</title>
    <link rel="stylesheet" type="text/css" href="style.css">
    <link href="webjars/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<nav class="navbar navbar-dark bg-dark">
    <a class="navbar-brand text-light" href="index.jsp">Little Twitter</a>
    <c:if test="${ requestScope.containsKey('message') }">
        <p class="text-light">${ requestScope.message.value }</p>
    </c:if>
    <c:choose>
        <c:when test="${ !sessionScope.containsKey('user') }">
            <form action="/login" method="post" class="form-inline">
                <input class="form-control mr-sm-2" type="search" name="login" placeholder="Login" aria-label="Login">
                <input class="form-control mr-sm-2" type="password" name="password" placeholder="Password"
                       aria-label="Password">
                <button type="submit" class="btn btn-outline-success my-2 my-sm-0">Login</button>
            </form>
        </c:when>
        <c:otherwise>
            <a class="btn btn-light" href="/logout" role="button">Log out</a>
        </c:otherwise>
    </c:choose>
</nav>
</body>
<div class="jumbotron jumbotron-fluid">
    <div class="row justify-content-md-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    ${article.articleTitle}
                </div>
                <div class="card-body">
                    <blockquote class="blockquote mb-0">
                        <p>${article.articleContent}</p>

                        <form method="post">
                            <textarea class="form-control" type="text" placeholder="Edit your article..."
                                      name="editContent" rows="3"></textarea>
                            <button type="submit" class="btn btn-secondary">Edit</button>
                        </form>
                        <footer class="blockquote-footer">${article.articleCreator.name}
                            <cite title="Source Title">${article.creationDate}</cite></footer>
                    </blockquote>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="webjars/jquery/3.2.1/jquery.min.js"></script>
<script src="webjars/bootstrap/4.1.3/js/bootstrap.min.js"></script>
</html>