<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; UTF-8" isELIgnored="false" pageEncoding="UTF-8" %>
<!doctype html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <title>Little Twitter</title>
    <link rel="stylesheet" type="text/css" href="style.css">
    <link href="webjars/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<c:if test="${!requestScope.containsKey('articles')}">
    <jsp:forward page="/articles"/>
</c:if>
<nav class="navbar navbar-dark bg-dark">
    <a class="navbar-brand text-light">Little Twitter</a>
    <c:if test="${ requestScope.containsKey('message') }">
        <p class="text-light">${ requestScope.message.value }</p>
    </c:if>
    <c:choose>
        <c:when test="${ !sessionScope.containsKey('user') }">
            <form action="/login" method="post" class="form-inline">
                <input class="form-control mr-sm-2" type="search" name="login" placeholder="Login" aria-label="Login">
                <input class="form-control mr-sm-2" type="password" name="password" placeholder="Password"
                       aria-label="Password">
                <button type="submit" class="btn btn-light">Login</button>
            </form>
        </c:when>
        <c:otherwise>
            <a class="btn btn-light" href="/logout" role="button">Log out</a>
        </c:otherwise>
    </c:choose>
</nav>

<div class="jumbotron jumbotron-fluid">
    <div class="row justify-content-md-center">
        <div class="col-md-6">
            <form>
                <div class="form-group">
                    <label>Email address</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
                </div>
                <div class="form-group form-check">
                    <input type="checkbox" class="form-check-input" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">Check me out</label>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>

<script src="webjars/jquery/3.2.1/jquery.min.js"></script>
<script src="webjars/bootstrap/4.1.3/js/bootstrap.min.js"></script>
</body>
</html>