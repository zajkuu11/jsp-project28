<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; UTF-8" isELIgnored="false" pageEncoding="UTF-8" %>
<!doctype html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <title>Little Twitter</title>
    <link rel="stylesheet" type="text/css" href="style.css">
    <link href="webjars/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<c:if test="${!requestScope.containsKey('articles')}">
    <jsp:forward page="/articles"/>
</c:if>
<nav class="navbar navbar-dark bg-dark">
    <a class="navbar-brand text-light">Little Twitter</a>
    <c:if test="${ requestScope.containsKey('message') }">
        <p class="text-light">${ requestScope.message.value }</p>
    </c:if>
    <c:choose>
        <c:when test="${ !sessionScope.containsKey('user') }">
            <form action="/login" method="post" class="form-inline">
                <input class="form-control mr-sm-2" type="search" name="login" placeholder="Login" aria-label="Login">
                <input class="form-control mr-sm-2" type="password" name="password" placeholder="Password"
                       aria-label="Password">
                <div class="btn-group" role="group">
                    <button type="submit" class="btn btn-light">Login</button>
                    <a class="btn btn-light" href="register.jsp" role="button">Register</a>
                </div>
            </form>
        </c:when>
        <c:otherwise>
            <a class="btn btn-light" href="/logout" role="button">Log out</a>
        </c:otherwise>
    </c:choose>
</nav>

<div class="jumbotron jumbotron-fluid">
    <div class="row justify-content-md-center">
        <div class="col-md-6">
            <c:if test="${sessionScope.containsKey('user')}">
                <form method="post" action="/newArticle">
                    <div class="input-group mb-3">
                        <textarea class="form-control" type="text" placeholder="Add new article..."
                                  name="newArticle" rows="3"></textarea>
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-secondary">Add</button>
                        </div>
                    </div>
                </form>
            </c:if>
            <c:forEach items="${articles}" var="article">
                <div class="card">
                    <div class="card-header">
                            ${article.articleTitle}
                    </div>
                    <div class="card-body">
                        <blockquote class="blockquote mb-0">
                            <p>${article.articleContent}</p>
                            <c:if test="${article.articleCreator.id == sessionScope.user.id}">
                                <footer class="blockquote-footer">${article.articleCreator.name}
                                    <cite title="Source Title">${article.creationDate}</cite></footer>
                                <c:url value="/delete" var="deletePostURL">
                                    <c:param name="id" value="${article.id}"/>
                                </c:url>
                                <a class="btn btn-secondary" href="${deletePostURL}" role="button">Usuń</a>

                                <c:url value="/update" var="editPostURL">
                                    <c:param name="update" value="${article.id}"/>
                                </c:url>
                                <a class="btn btn-secondary" href="${editPostURL}" role="button">Edytuj</a>
                            </c:if>

                        </blockquote>
                    </div>
                </div>
            </c:forEach>
        </div>
    </div>
</div>

<script src="webjars/jquery/3.2.1/jquery.min.js"></script>
<script src="webjars/bootstrap/4.1.3/js/bootstrap.min.js"></script>
</body>
</html>