package com.example;

import com.example.mvc.model.Article;
import com.example.mvc.model.Gender;
import com.example.mvc.model.Role;
import com.example.mvc.model.User;
import com.google.common.collect.Lists;

import java.time.LocalDate;
import java.util.List;

public class Database {

    private List<User> users;
    private List<Article> articles;
    private static Database instance = null;

    private Database() {
        this.users = initUsers();
        this.articles = initArticles();
    }

    public static Database getInstance() {
        if (instance == null) {
            instance = new Database();
        }
        return instance;
    }

    private List<User> initUsers() {
        List<User> result = Lists.newArrayList();
        result.add(new User(1L, "user", "user", Role.ROLE_USER, "Jan", "jan123@mail",
                LocalDate.of(1990, 5, 18), Gender.MALE, true));

        result.add(new User(2L, "test", "test", Role.ROLE_USER, "Piotr", "piotr11@mail",
                LocalDate.of(1982, 2, 27), Gender.MALE, true));

        result.add(new User(3L, "admin", "admin", Role.ROLE_ADMIN, "Anna", "anna11@mail",
                LocalDate.of(1982, 8, 3), Gender.FEMALE, false));
        return result;
    }

    public List<User> getUsers() {
        return users;
    }

    private List<Article> initArticles() {
        List<Article> result = Lists.newArrayList();
        result.add(new Article(1L, "witaj", "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
                LocalDate.of(1992, 11, 23), users.get(2)));
        result.add(new Article(2L, "heh", "bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb",
                LocalDate.of(1999, 1, 15), users.get(0)));
        result.add(new Article(3L, "qwe", "cccccccccccccccccccccccccccccccccccccccccccc",
                LocalDate.of(2014, 9, 11), users.get(2)));
        return result;
    }

    public List<Article> getArticles() {
        return articles;
    }

}
