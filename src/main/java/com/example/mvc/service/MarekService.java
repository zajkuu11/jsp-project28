package com.example.mvc.service;

import com.example.mvc.model.Marek;
import com.example.mvc.repository.MarekRepository;

public class MarekService {
    private MarekRepository marekRepository;
    private static MarekService instance = null;


    private MarekService(){
        this.marekRepository = MarekRepository.getInstance();
    }

    public static MarekService getInstance(){
        if (instance == null){
            instance = new MarekService();
        }
        return instance;
    }

    public Long save(Marek marek){
        return marekRepository.save(marek);
    }

}
