package com.example.mvc.service;

import com.example.Database;
import com.example.mvc.model.Article;
import com.example.mvc.model.User;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.Random;

public class ArticleService {
    private List<Article> articles;

    public ArticleService() {
        this.articles = Database.getInstance().getArticles();
    }

    public List<Article> getArticles() {
        return articles;
    }

    public Article getArticleByID(Long id){
        Optional<Article> foundArticle = articles.stream().filter(article -> article.getId().equals(id)).findFirst();
        return foundArticle.orElse(null);
    }

    public boolean deleteArticle(Long id) {
        Optional<Article> optArticle = articles.stream().filter(article -> article.getId().equals(id)).findFirst();
        if (optArticle.isPresent()) {
            articles.remove(optArticle.get());
            return true;
        } else return false;
    }
    public boolean updateArticle(Long id, String content){
        Optional<Article> optArticle = articles.stream().filter(article -> article.getId().equals(id)).findFirst();
        if (optArticle.isPresent()) {
            optArticle.get().setArticleContent(content);
            return true;
        } else return false;
    }
    public void addArticle(User user, String content){
        articles.add(articles.size(), new Article(new Random().nextLong(), "nvm",content , LocalDate.now(), user));
    }
}
