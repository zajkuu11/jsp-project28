package com.example.mvc.repository;


import com.example.mvc.model.Marek;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class MarekRepository extends AbstractRepository {

    private static MarekRepository instance = null;

    private MarekRepository() {
    }

    public static MarekRepository getInstance(){
        if (instance == null){
            instance = new MarekRepository();
        }
        return instance;
    }
    public Long save(Marek marek) {
        Session session = getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        Long id = (Long) session.save(marek);

        transaction.commit();
        session.close();
        return id;
    }

}
