package com.example.mvc.repository;

import com.example.mvc.model.Marek;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class AbstractRepository {
    public SessionFactory getSessionFactory(){
        Configuration configuration = new Configuration()
                .addAnnotatedClass(Marek.class)
                .configure();

        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().
                applySettings(configuration.getProperties()).build();
        return configuration.buildSessionFactory(serviceRegistry);
    }
}
