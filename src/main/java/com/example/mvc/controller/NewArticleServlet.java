package com.example.mvc.controller;

import com.example.mvc.model.User;
import com.example.mvc.service.ArticleService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "newArticle", value = "/newArticle")
public class NewArticleServlet extends HttpServlet {
    ArticleService articleService;
    @Override
    public void init() throws ServletException {
        articleService = new ArticleService();
    }
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
 /*       String newArticle = req.getParameter("newArticle");
        Object user = req.getSession().getAttribute("user");
        System.out.println(newArticle + " " + user.toString());
        req.getRequestDispatcher("/").forward(req, resp);*/
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String newArticle = req.getParameter("newArticle");
        User user = (User)req.getSession().getAttribute("user");
        System.out.println(newArticle + " " + user.toString());
        articleService.addArticle((User)req.getSession().getAttribute("user"), newArticle);
        req.getRequestDispatcher("/").forward(req, resp);
    }
}
