package com.example.mvc.controller;


import com.example.mvc.model.Article;
import com.example.mvc.service.ArticleService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "articleUpdate", value = "/update")
public class ArticleUpdateServlet extends HttpServlet {
    private ArticleService articleService;

    @Override
    public void init() throws ServletException {
        articleService = new ArticleService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long id = Long.parseLong(req.getParameter("update"));
        System.out.println(id);
        Article article = articleService.getArticleByID(id);
        req.setAttribute("article", article);
        req.getRequestDispatcher("edit.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String editContent = req.getParameter("editContent");
        Article article = articleService.getArticleByID(Long.parseLong(req.getParameter("update")));
        article.setArticleContent(editContent);
        req.getRequestDispatcher("index.jsp").forward(req, resp);
    }
}
