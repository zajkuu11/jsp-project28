package com.example.mvc.controller;

import com.example.mvc.service.ArticleService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet(name = "articles", urlPatterns ={ "/articles"})
public class ArticleServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ArticleService articles = new ArticleService();
        req.setAttribute("articles", articles.getArticles());
        req.getRequestDispatcher("/index.jsp").forward(req, resp);

    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ArticleService articles = new ArticleService();
        req.setAttribute("articles", articles.getArticles());
        req.getRequestDispatcher("/index.jsp").forward(req, resp);
    }
}
