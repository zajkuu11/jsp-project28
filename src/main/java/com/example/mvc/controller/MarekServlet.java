package com.example.mvc.controller;

import com.example.mvc.model.Marek;
import com.example.mvc.service.ArticleService;
import com.example.mvc.service.MarekService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet(name = "MarekServlet", value = "/dodajMarka")
public class MarekServlet extends HttpServlet {
    private  MarekService marekService;

    @Override
    public void init() throws ServletException {
        this.marekService = MarekService.getInstance();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Marek marek = new Marek("marek");
        Marek marek2 = new Marek("mareczek");
        marekService.save(marek);
        marekService.save(marek2);
    }
}
