package com.example.mvc.model;

import java.io.Serializable;
import java.time.LocalDate;

public class Article implements Serializable {
    private Long id;
    private String articleTitle;
    private String articleContent;
    private LocalDate creationDate;
    private User articleCreator;

    public Article() {
    }

    public Article(Long id, String articleTitle, String articleContent, LocalDate creationDate, User articleCreator) {
        this.id = id;
        this.articleTitle = articleTitle;
        this.articleContent = articleContent;
        this.creationDate = creationDate;
        this.articleCreator = articleCreator;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getArticleTitle() {
        return articleTitle;
    }

    public void setArticleTitle(String articleTitle) {
        this.articleTitle = articleTitle;
    }

    public String getArticleContent() {
        return articleContent;
    }

    public void setArticleContent(String articleContent) {
        this.articleContent = articleContent;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public User getArticleCreator() {
        return articleCreator;
    }

    public void setArticleCreator(User articleCreator) {
        this.articleCreator = articleCreator;
    }

    @Override
    public String toString() {
        return "Article{" +
                "id=" + id +
                ", articleTitle='" + articleTitle + '\'' +
                ", articleContent='" + articleContent + '\'' +
                ", creationDate=" + creationDate +
                ", articleCreator=" + articleCreator +
                '}';
    }
}
